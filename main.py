from array import array


if __name__ == '__main__':
    my_int_array = array('i', [10, 4, 3, 7, 9, 15])
    print(my_int_array)
    print(my_int_array[2])
    my_int_array.append(20)
    print(my_int_array)
    # next 2 lines produce an error: TypeError
#    my_int_array.append(15.0)  # 'float' object cannot be interpreted as  an integer
#    my_int_array.append('abc')  # string also cannot be added to int array
    my_int_array.append(True)  # bool is converted to int; True -> 1
    print(my_int_array)

    popped_item = my_int_array.pop(0)
    print('popped element', popped_item)
    print('array after pop', my_int_array)

    # write array to the file
    with open('my_array.bin', 'wb') as file:
        my_int_array.tofile(file)

    # read array from the file
    imprted_array = array('i')
    with open('my_array.bin', 'rb') as file:
        imprted_array.fromfile(file, 3)
    print(imprted_array)
