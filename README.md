# Chapter 62: Other Built-in Modules

This is sample Python project to demonstrate simple usage of Python's modules.

## Built-in array Module

`main.py` file contains example usage of built-in `array` module.

To see it in action, execute following command:
```shell
python main.py
```

## Accessing Program Arguments using the built-in sys Module

`my_args.py` file contains example usage of built-in `sys` module to read command line arguments.

To see it in action, execute following command:
```shell
python my_args.py user password
```

This application will read arguments passed to this command.

## Built-in webbrowser Module

`my_browser.py` file contains example usage of built-in `webbrowser` module to launch web browser.

To see it in action, execute following command:
```shell
python my_browser.py
```
