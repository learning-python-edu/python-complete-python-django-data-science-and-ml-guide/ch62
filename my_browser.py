import webbrowser


def search_on_search_engine(query, search_engine):
    search_engines = {
        'google': 'https://www.google.com/search?q={}',
        'yahoo': 'https://search.yahoo.com/search?p={}',
        'bing': 'https://www.bing.com/search?q={}',
    }

    if search_engine not in search_engines:
        print('Unsupported search engine:', search_engine)
        return

    search_url = search_engines[search_engine].format(query)
    webbrowser.open(search_url)


if __name__ == '__main__':
    # webbrowser.open('https://pypi.org')
    search_on_search_engine('Python programming', search_engine='facebook')
    search_on_search_engine('Python programming', search_engine='google')
